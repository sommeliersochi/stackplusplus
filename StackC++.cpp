#include <iostream>

const int SIZE = 25; // � ���� �������� 26 ����.
using namespace std;


template <typename SType> class stack {
	SType stck[SIZE];
	int tos;
public:
	stack();
	~stack();
	void push(SType i);
	SType pop();
};

template <typename SType> stack<SType>::stack() //����������� stack
{
	tos = 0;
	cout << "Initialized\n";
}

template <typename SType> stack<SType>::~stack() //���������� stack 
{
	cout << "Destroyed\n";
}

template <typename SType> void stack<SType>::push(SType i) // � push(), ����� �������� ����� �������.
{
	if (tos == SIZE) {
		cout << "Stack is full. \n";
		return;
	}
	stck[tos] = i;
	tos++;
}

template <typename SType> SType stack<SType>::pop() // ������ pop(), ����� ������� �� ����� ������� �������
{
	if (tos == 0) {
		cout << "Stack underflow.\n";
		return 0;
	}
	tos--;
	return stck[tos];
}
int main()
{
	stack<int> a; // ����������� ��� ������ ���� ������ �� ����� int
	stack<double> b; // ����������� ��� ������ ���� ������ �� ����� double
	stack<char> �; //����������� ��� ������ ���� ������ �� ����� char
	int i;
	// ������������� int � double ������
	a.push(1);
	b.push(20.5);
	a.push(2);
	b.push(-12.23);
	a.push(3);
	b.push(2000.01);

	cout << a.pop() << "\n";
	cout << a.pop() << "\n";
	cout << a.pop() << "\n";
	cout << b.pop() << "\n";
	cout << b.pop() << "\n";
	cout << b.pop() << "\n";
	// ������������ ����������� �����
	for (i = 0; i < 26; i++) �.push((char)'A' + i);
	for (i = 0; i < 26; i++) cout << �.pop() << " ";
	cout << "\n";
	return 0;
}